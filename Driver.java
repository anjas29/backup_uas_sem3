import java.util.ArrayList;
import java.util.Scanner;

public class Driver{
	int type;
	double money;
	Vertex position;
	Vertex customerDestination;
	double distanceCost;
	ArrayList<WishList> wishList;
	ArrayList<Item> items;

	Driver(Vertex v){
		position = v;
		distanceCost = 0;
		wishList = new ArrayList<WishList>();
		items = new ArrayList<Item>();
		type = 2;
	}
	public void changeState(){
		if(type==2){
			type = 1;
			System.out.println("\n~ Pengemudi Manual ~~~");
		}else {
			type = 2;
			System.out.println("\n~ Pengemudi Otomatis ~~~");
		}
	}
	public int getMode(){
		return type;
	}
	public void getStatus(){
		System.out.println("\n~Status Pengemudi  ~~~");
		System.out.println("~ Posisi \t\t: " + position.nama);
		System.out.println("~ Modal \t\t: " + money);
		System.out.println("~ Jarak Tempuh \t\t: " + distanceCost);
		System.out.println("~ Biaya/Jarak Tempuh \t: "+ distanceCost*500);
		System.out.print("~ Mode Pengemudi \t: ");
		if(type==2){
			System.out.println("Otomatis");
		}else System.out.println("Manual");
	}

	public void getWishList(){
		if(wishList.size() != 0){
			System.out.println("\n~Daftar Pesanan  ~~~");
			System.out.println("~ Alamat Pelanggan : "+customerDestination.nama);
			System.out.println("\n~ Pesanan : ");
			System.out.printf("  %-15s %-20s %-12s %-6s   Cek\n","Lokasi", "Makanan", "Harga", "Jumlah");
			for(WishList w :wishList){
				System.out.printf("~ %-15s %-20s %-12s %-6s",w.location.nama, w.menu.nama, "Rp "+	w.menu.harga, w.quantity);
				if(w.checked) System.out.println("   V");
				else System.out.println("   X");
			}
			System.out.println("\n");
		}else System.out.println("~ Daftar Pesanan Kosong");
		
	}

	public void resetDriverStatus(){
		money = 100000;
		distanceCost = 0;
	}
	public void setPosition(Edge e){
		position = e.destination;
		addCost(e.cost);
	}

	public void setPosition(Vertex v, double d){
		position = v;
		addCost(d);
	}

	public void resetDistanceCost(){
		distanceCost = 0;
	}
	public void setWishList(ArrayList<WishList> wishList){
		this.wishList = wishList;
	}

	public Vertex getCurrent(){
		return position;
	}
	public void addCost(double cost){
		distanceCost += cost;
	}
	public void addWishList(WishList wishList){
		int tempTotal = 0;
		for(WishList w : this.wishList){
			tempTotal += w.menu.harga*w.quantity;
		}
		if(tempTotal+wishList.menu.harga*wishList.quantity < 100000){
			this.wishList.add(wishList);	
		}else{
			System.out.println("\n~Pemesanan melebihi kapasitas\n");
		}
		
	}
	public void clearWishList(){
		this.wishList.clear();
	}
	public void buyItem(Menu menu, int quantity){
		double tempCost = money - (menu.harga*quantity);

		if(tempCost >= 0){
			money = tempCost;
			boolean check2 = false;
			for(){
				
			}
			if(check2)items.add(new Item(menu, quantity));
			System.out.println("~ Pembelian berhasil");
			checkWishList();
		}else{
			System.out.println("\n~ Uang tidak mencukupi");
		}
	}

	public void serveCustomer(){

		double price = 0;
		if(position == customerDestination){
			boolean check = true;
			for(WishList w : wishList){
				if(!w.checked) check = false;
			}
			if(check){
				for(Item i : items){
					price += i.menu.harga*i.quantity;
				}
				money += price;
				double total = price+(distanceCost*500); 
				System.out.println("\n~Transaksi Berhasil");
				System.out.print("~ Pembayaran  (Rp "+total+") : ");
				Scanner input = new Scanner(System.in);

				double payment = input.nextDouble();
				if(payment>=total){
					System.out.println("\t\t\t~~~~~   ABC    ~~~~~\n");
					System.out.printf(" ~ %-20s %-10s %-7s %-10s\n", "Makanan","Harga","Jumlah", "Total");
					for(Item i : items){
						System.out.printf(" ~ %-20s %-10s %-7s Rp %-10s\n", i.menu.nama, i.menu.harga, i.quantity, i.menu.harga*i.quantity);
					}
					System.out.println("\n");
					System.out.printf(" ~ %-20s %-10s %-7s Rp %-10s\n", "Total Biaya Pembelian","","",price);

					System.out.printf("\n ~ %-20s %-10s %-7s Rp %-10s\n", "Total Biaya Jasa", 500, distanceCost, distanceCost*500);
					System.out.printf("\n ~ %-20s %-10s %-7s Rp %-10s\n", "Total Biaya ","" ,"" , total);
					System.out.printf("\n ~ %-20s %-10s %-7s Rp %-10s\n", "Pembayaran ","" ,"" , payment);
					System.out.printf("\n ~ %-20s %-10s %-7s Rp %-10s\n", "Kembalian ","" ,"" , payment-total);
					money += distanceCost*500;
					distanceCost = 0;
					wishList.clear();
					customerDestination = null;
				}else System.out.println("~Pembayaran Kurang!!");
				
			}else{
				System.out.println("\n~ Barang tidak valid");
			}
		}else{
			System.out.println("\n~ Pelanggan tidak benar!");
		}

	}
	public void checkWishList(){
		for(WishList w : wishList){
			if(!w.checked){
				for(Item i : items){
					if(i.menu == w.menu && i.quantity >= w.quantity){
						w.checked = true;
					}else{
						
					}
				}
			}
		}
	}
	public void setCustomerDestination(Vertex customerDestination){
		this.customerDestination = customerDestination;
		this.wishList.clear();
		this.items.clear();
	}
}

