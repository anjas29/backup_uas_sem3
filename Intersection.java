import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;


class Intersection extends Vertex{
	Intersection(String nama){
		super(nama, MngVertex.INTERSECTION);
	}
}

class Customer extends Vertex{
	String alamat;
	ArrayList<WishList> wishList;

	Customer(String nama){
		super(nama, MngVertex.CUSTOMER);
		wishList = new ArrayList<WishList>();
	}

}

class FoodShop extends Vertex{
	String  alamat;
	ArrayList<Menu> menu;
	FoodShop(String nama){
		super(nama, MngVertex.FOODSHOP);
		menu = new ArrayList<Menu>();
	}
}

class ABCorp extends Vertex{
	String alamat;
	ABCorp(String nama){
		super(nama, MngVertex.ABCORP);
	}
}