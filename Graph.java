import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Collections;

public class Graph{
	Vertex [] v = new Vertex[31];
	ArrayList<Vertex> tempPath = new ArrayList<Vertex>();
	Vertex target;
	Vertex begin;
	int longCost = Integer.MIN_VALUE;
	int cost = 0;
	int pos = 0;
	ArrayList<Vertex> arrayTempPath = new ArrayList<Vertex>();

	Vertex currentVertex = v[0];
	
	public Vertex getCurrent(){
		return currentVertex;
	}

	public Vertex getRoot(){
		return v[0];
	}
	public void setCurrent(Vertex v){
		currentVertex = v;
	}

	public ArrayList<Edge> getDestinationFrom(Vertex v){
		ArrayList<Edge> temp = new ArrayList<Edge>();

		for(Edge e : v.edges){
			temp.add(e);
		}

		return temp;
	}
	public void createMap(){
		v[0] = new ABCorp("ABCorp");
		v[1] = new Intersection("Persimpangan Jl. Strawberry - Jl. Merpati");
		v[2] = new Intersection("Persimpangan Jl. Cemara - Jl. Merpati - Gg. Pisang");
		v[3] = new Intersection("Persimpangan Jl. Perkutut - Gg. Pisang - Jl. MT. Haryono");
		v[4] = new Customer("Rumah 1 Jl. MT. Haryono");
		v[5] = new Intersection("Persimpangan Jl. MT. Haryono - Jl. Patimura - Jl. Kenari");
		v[6] = new Customer("Rumah 1 Jl. Kenari");
		v[7] = new Customer("Rumah 1 Jl. Perkutut");
		v[8] = new Customer("Rumah 2 Jl. Perkutut");
		v[9] = new Intersection("Persimpangan Jl. Muh Hatta - Jl. Perkutut");
		v[10] = new Intersection("Persimpangan Jl. Kenangan - Jl. Cemara - Jl. Muh Hatta");
		v[11] = new Intersection("Persimpangan Jl. Petung - Jl. Kedondong - Jl. Kenangan");
		v[12] = new Customer("Rumah 2 Jl. Kedondong");
		v[13] = new FoodShop("Kebab");
		v[14] = new FoodShop("Ayam Geprek");
		v[15] = new Intersection("Persimpangan Jl. Kedondong - Jl. Tanpa Nama - Jl. Supratman - Jl. Patimura");
		v[16] = new Customer("Rumah 1 Jl. Patimura");
		v[17] = new FoodShop("Burger");
		v[18] = new Intersection("Persimpangan Jl. Supratman - Jl. Manggis");
		v[19] = new FoodShop("Sate Ayam");
		v[20] = new Customer("Rumah 1 Jl. Manggis");
		v[21] = new Intersection("Persimpangan Jl. Affandi - Jl. Manggis");
		v[22] = new FoodShop("Lontong Opor");
		v[23] = new FoodShop("Pizza");
		v[24] = new Intersection("Persimpangan Jl. Affandi - Jl. Salak");
		v[25] = new Customer("Rumah 1 Jl. Salak");
		v[26] = new Intersection("Persimpangan Jl. Pakis - Jl. Salak - Jl. Melati");
		v[27] = new Intersection("Persimpangan Jl. Pakis - Jl. Petung");
		v[28] = new FoodShop("Juice");
		v[29] = new Customer("Rumah 1 Jl. Melati");
		v[30] = new Intersection("Persimpangan Jl. Melati - Jl. Tanpa Nama");

		v[0].edges = new Edge[]{
			new Edge(5, v[1])
		};
		v[1].edges = new Edge[]{
			new Edge(5, v[0]), new Edge(5, v[2])
		};
		v[2].edges = new Edge[]{
			new Edge(5, v[1]), new Edge(10, v[10]), new Edge(5, v[3])
		};
		v[3].edges = new Edge[]{
			new Edge(3, v[4]), new Edge(5, v[2])
		};
		v[4].edges = new Edge[]{
			new Edge(7, v[5])
		};
		v[5].edges = new Edge[]{
			new Edge(8, v[6]), new Edge(6, v[17])
		};
		v[6].edges = new Edge[]{
			new Edge(8, v[5])
		};
		v[7].edges = new Edge[]{
			new Edge(4, v[3])
		};
		v[8].edges = new Edge[]{
			new Edge(4, v[7])
		};
		v[9].edges = new Edge[]{
			new Edge(2, v[8]), new Edge(5, v[10])
		};
		v[10].edges = new Edge[]{
			new Edge(5, v[9]), new Edge(10, v[2]), new Edge(15, v[11])
		};
		v[11].edges = new Edge[]{
			new Edge(15, v[10]), new Edge(5, v[12]), new Edge(10, v[27])
		};
		v[12].edges = new Edge[]{
			new Edge(5, v[11]), new Edge(5, v[13])
		};
		v[13].edges = new Edge[]{
			new Edge(5, v[12]), new Edge(5, v[14])
		};
		v[14].edges = new Edge[]{
			new Edge(5, v[13]), new Edge(5, v[15])
		};
		v[15].edges = new Edge[]{
			new Edge(5, v[14]), new Edge(10, v[30]), new Edge(10, v[18]), new Edge(6, v[16])
		};
		v[16].edges = new Edge[]{
			new Edge(6, v[15]), new Edge(8, v[17])
		};
		v[17].edges = new Edge[]{
			new Edge(8, v[16]), new Edge(6, v[5])
		};
		v[18].edges = new Edge[]{
			new Edge(10, v[15]), new Edge(10, v[19])
		};
		v[19].edges = new Edge[]{
			new Edge(10, v[18]), new Edge(7, v[20])
		};
		v[20].edges = new Edge[]{
			new Edge(7, v[19]), new Edge(3, v[21])
		};
		v[21].edges = new Edge[]{
			new Edge(3, v[20]), new Edge(5, v[22])
		};
		v[22].edges = new Edge[]{
			new Edge(5, v[21]), new Edge(5, v[23])
		};
		v[23].edges = new Edge[]{
			new Edge(5, v[22]), new Edge(10, v[24])
		};
		v[24].edges = new Edge[]{
			new Edge(10, v[23]), new Edge(7, v[25])
		};
		v[25].edges = new Edge[]{
			new Edge(7, v[24]), new Edge(8, v[26])
		};
		v[26].edges = new Edge[]{
			new Edge(8, v[25]), new Edge(5, v[27]), new Edge(4, v[28])
		};
		v[27].edges = new Edge[]{
			new Edge(10, v[11]), new Edge(5, v[26])
		};
		v[28].edges = new Edge[]{
			new Edge(4, v[26]), new Edge(7, v[29])
		};
		v[29].edges = new Edge[]{
			new Edge(7, v[28]), new Edge(4, v[30])
		};
		v[30].edges = new Edge[]{
			new Edge(4, v[29]), new Edge(10, v[15])
		};
	}

	public void createMenus(){
		((FoodShop) v[13]).menu.add(new Menu("Bread Kebab", 8000));
		((FoodShop) v[13]).menu.add(new Menu("Kebab Junior", 9000));
		((FoodShop) v[13]).menu.add(new Menu("Kebab Sosis", 9000));
		((FoodShop) v[13]).menu.add(new Menu("Kebab Beefull", 10000));
		((FoodShop) v[13]).menu.add(new Menu("Kebab Jumbo", 12000));

		((FoodShop) v[14]).menu.add(new Menu("Geprek Goreng", 8000));
		((FoodShop) v[14]).menu.add(new Menu("Geprek Goreng Crispy", 9000));

		((FoodShop) v[17]).menu.add(new Menu("Small Chicken Burger", 10000));
		((FoodShop) v[17]).menu.add(new Menu("Big Chicken Burger", 12000));
		((FoodShop) v[17]).menu.add(new Menu("Small Beef Burger", 12000));
		((FoodShop) v[17]).menu.add(new Menu("Big Beef Burger", 14000));

		((FoodShop) v[19]).menu.add(new Menu("Sate Ayam Manis", 13000));
		((FoodShop) v[19]).menu.add(new Menu("Sate Ayam Pedas", 14000));
		((FoodShop) v[19]).menu.add(new Menu("Lalapan", 3000));

		((FoodShop) v[22]).menu.add(new Menu("Lontong Opor Porsi Kecil", 10000));
		((FoodShop) v[22]).menu.add(new Menu("Lontong Opor Porsi Keluarga", 20000));

		((FoodShop) v[23]).menu.add(new Menu("Pan Pizza", 15000));
		((FoodShop) v[23]).menu.add(new Menu("Crown Crust", 25000));
		((FoodShop) v[23]).menu.add(new Menu("Chessy Bites", 27000));

		((FoodShop) v[28]).menu.add(new Menu("Orange Juice", 5000));
		((FoodShop) v[28]).menu.add(new Menu("Strawberry Juice", 6000));
		((FoodShop) v[28]).menu.add(new Menu("Strawberry Juice", 6000));
		((FoodShop) v[28]).menu.add(new Menu("Melon Juice", 6000));
		((FoodShop) v[28]).menu.add(new Menu("Hot Milk", 6000));
		((FoodShop) v[28]).menu.add(new Menu("Ice Milk", 7000));
		((FoodShop) v[28]).menu.add(new Menu("Avocado Juice", 8000));

	}


	public ArrayList<Vertex> getShortestPathTo(Vertex source, Vertex target){
		resetMinDistance();
		source.minDistance = 0;
		source.previous = null;
		Queue<Vertex> vertexQueue = new LinkedList<Vertex>();
		vertexQueue.add(source);

		while(!vertexQueue.isEmpty()){
			Vertex u = vertexQueue.poll();
			for(Edge e : u.edges){
				Vertex v = e.destination;
				double weight = e.cost;
				double distance = u.minDistance + weight;
				if(distance<v.minDistance){
					vertexQueue.remove(v);
					v.minDistance = distance;
					v.previous = u;
					vertexQueue.add(v);
				}
			}
		}
		return listSortestPath(target);
	}

	public ArrayList<Vertex> listSortestPath(Vertex destination){
		ArrayList<Vertex> path = new ArrayList<Vertex>();
		for(Vertex v = destination; v != null; v = v.previous){
			path.add(v);
		}
		Collections.reverse(path);
		return path;
	}

	public ArrayList<Vertex> getLongestPath(Vertex start, Vertex end){
		begin = start;
		target = end;
		cost = 0;
		dfsLongestPath(begin);

		return tempPath;
	}

	public void dfsLongestPath(Vertex current){
		current.visited = true;
		arrayTempPath.add(current);
		pos++;
		if(current == target && cost > longCost ){
			tempPath.clear();
			target.maxDistance = cost;
			for(Vertex v : arrayTempPath){
				if(v != null) tempPath.add(v);
			}
			longCost = cost;
		} else {
			for(Edge e : current.edges){
				if(!e.destination.visited){
					cost += e.cost;
					dfsLongestPath(e.destination);
					cost -= e.cost;
				}
			}
		}
		current.visited = false;
		arrayTempPath.remove(--pos);
	}

	

	public ArrayList<Vertex> getArrayLocation(int type, Vertex current){
		ArrayList<Vertex> temp = new ArrayList<Vertex>();
		for(Vertex vertex : this.v){
			if(vertex != null && vertex != current && vertex.getType() == type){
				temp.add(vertex);
			}
		}
		if(temp.size() == 0){
			System.out.println("\n~ Dafta Kosong ~~~");
		}
		return temp;
	}

	public void resetMinDistance(){
		for(Vertex vertex : this.v){
			if(vertex != null) {
				vertex.minDistance = Double.POSITIVE_INFINITY;
			}
		}
	}

}
