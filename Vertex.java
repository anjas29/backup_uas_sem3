import java.util.ArrayList;

interface MngVertex{
	final int INTERSECTION = 3;
	final int CUSTOMER = 2;
	final int FOODSHOP = 1;
	final int ABCORP = 4;

	final int MAX_VALUE = Integer.MAX_VALUE;
}

class Vertex implements Comparable<Vertex>{
	String nama;
	int type;
	Edge [] edges;
	String address;
	double minDistance;
	double maxDistance;
	Vertex previous;
	boolean visited;

	Vertex(String nama, int type){
		
		this.nama = nama;
		this.type = type;
		minDistance = Double.POSITIVE_INFINITY;
		visited = false;
	}

	public int getType(){
		return type;
	}

	public int compareTo(Vertex v){
	    return Double.compare(minDistance, v.minDistance);
	}


}