
class WishList{
	Menu menu;
	int quantity;
	Vertex location;
	boolean checked;
	WishList(Vertex location, Menu menu, int quantity){
		this.location = location;
		this.menu  = menu;
		this.quantity = quantity;
		this.checked = false;
	}
}

class Item{
	Menu menu;
	int quantity;

	Item(Menu menu, int quantity){
		this.menu = menu;
		this.quantity = quantity;
	}
}