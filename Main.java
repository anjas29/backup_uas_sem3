import java.util.ArrayList;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Scanner;

public class Main{
	
	public static void main(String args[]){
		Graph graph = new Graph();
		graph.createMap();
		graph.createMenus();
		int menu = 0;
		Driver driver = new Driver(graph.getRoot());
		ArrayList<Edge> tempDestination = new ArrayList<Edge>();
		ArrayList<Vertex> tempDestinationVertex = new ArrayList<Vertex>();
		ArrayList<Vertex> tempRoute = new ArrayList<Vertex>();
		
 
		System.out.print("\n--- Program ABCorp ---");

		while(true){
		switch(driver.getCurrent().getType()){
			case MngVertex.ABCORP :
				driver.resetDriverStatus();
				System.out.println("\n\nABCorp");
				System.out.println("~Posisi: " + driver.getCurrent().nama);
				System.out.println("  1. Pemesanan\n  2. Lanjutkan Perjalanan\n  3. Cek Status\n  4. Cek Pesanan\n  5. Ubah Mode");
				System.out.print("~Pilih : ");
				menu = getInt();

				switch(menu){
					case 1:
						System.out.println("\n~Tujuan Pelanggan ~~~");

						tempDestinationVertex = graph.getArrayLocation(MngVertex.CUSTOMER, graph.getRoot());
						printDestinationVertex(tempDestinationVertex);

						System.out.print("~Pilih : ");
						int dstCustomer = getInt()-1;
						driver.setCustomerDestination(tempDestinationVertex.get(dstCustomer));

						int selectFoodshop = 0;
						do{
							System.out.println("\n~Pemesanan ~~~");

							tempDestinationVertex = graph.getArrayLocation(MngVertex.FOODSHOP, graph.getRoot());
							printDestinationVertex(tempDestinationVertex);

							System.out.print("~Pilih (0 untuk batal) : ");
							selectFoodshop = getInt()-1;

							if(selectFoodshop != -1){
								FoodShop sFoodShop = getFoodShop(tempDestinationVertex.get(selectFoodshop));
								int in = 1;
								for(Menu e: sFoodShop.menu){
									System.out.println(in++ + ". "+ e.nama + " : "+e.harga);

								}
								System.out.print("~Pilih (0 untuk batal) : ");
								int selectMenu = getInt()-1;

								if(selectMenu != -1){
									if(selectMenu >= 0 && selectMenu < tempDestinationVertex.size()){
										System.out.print("~Jumlah : ");
										int quantity = getInt();
										driver.addWishList(new WishList(sFoodShop, sFoodShop.menu.get(selectMenu), quantity));
									}else{
										System.out.println("\nError | Pilihan tidak valid!");
									}
								}
							}

						}while(selectFoodshop != -1);

					break;
					case 2:
						switch(driver.getMode()){
							case 1:
								System.out.println("\n~Pengemudi Manual ~~~");
								tempDestination = graph.getDestinationFrom(driver.getCurrent());
								printDestination(tempDestination);
								System.out.print("~Pilih : ");
								int select = getInt()-1;
								driver.setPosition(tempDestination.get(select));
							break;
							case 2:
								System.out.println("\n~Pengemudi Otomatis ~~~");
								System.out.println("   1. Toko Makanan\n   2. Pelanggan\n   3. Persimpangan\n   4. ABCorp");
								System.out.print("~Pilih : ");
								menu = getInt();
								tempDestinationVertex = graph.getArrayLocation(menu, driver.getCurrent());
								System.out.println("\n~ Tujan ~~~~");
								printDestinationVertex(tempDestinationVertex);
								System.out.print("~Pilih : ");
								int select2 = getInt()-1;
								if(select2 >=0 && select2 <tempDestinationVertex.size()){
									System.out.println("\n~Pilih Driver:\n   1. Driver Cepat\n   2. Driver Lambat");
									System.out.print("~Pilih : ");
									int select3 = getInt();
									double tempTotal=0;
									switch(select3){
										case 1:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getShortestPathTo(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.minDistance);
													tempTotal = v.minDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										case 2:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getLongestPath(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.maxDistance);
													tempTotal = v.maxDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										default:
											System.out.println("Error | Invalid Menu");
									};
								}else System.out.println("\nError | Tujuan tidak valid!");
								
							break;
							default:
								System.out.println("Error | Invalid Menu");
							break;

						}
					break;
					case 3:
						driver.getStatus();
					break;
					case 4:
						driver.getWishList();
					break;
					case 5:	
						driver.changeState();
					break;
					default:
						System.out.println("Error | Invalid Menu ");
					break;
				}
			break;
			case MngVertex.INTERSECTION :
				System.out.println("\n\nPersimpangan");
				System.out.println("~Posisi: " + driver.getCurrent().nama + "");
				System.out.println("  1. Lanjutkan Perjalanan\n  2. Cek Status\n  3. Cek Pesanan\n  4. Ubah Mode ");
				System.out.print("~Pilih : ");
				menu = getInt();

				switch(menu){
					case 1:
						switch(driver.getMode()){
							case 1:
								System.out.println("\n~Pengemudi Manual ~~~");
								tempDestination = graph.getDestinationFrom(driver.getCurrent());
								printDestination(tempDestination);
								System.out.print("~Pilih : ");
								int select = getInt()-1;
								driver.setPosition(tempDestination.get(select));
							break;
							case 2:
								System.out.println("\n~Pengemudi Otomatis ~~~");
								System.out.println("   1. Toko Makanan\n   2. Pelanggan\n   3. Persimpangan\n   4. ABCorp");
								System.out.print("~Pilih : ");
								menu = getInt();
								tempDestinationVertex = graph.getArrayLocation(menu, driver.getCurrent());
								System.out.println("\n~ Tujan ~~~~");
								printDestinationVertex(tempDestinationVertex);
								System.out.print("~Pilih : ");
								int select2 = getInt()-1;
								if(select2 >=0 && select2 <tempDestinationVertex.size()){
									System.out.println("\n~Pilih Driver:\n   1. Driver Cepat\n   2. Driver Lambat");
									System.out.print("~Pilih : ");
									int select3 = getInt();
									double tempTotal=0;
									switch(select3){
										case 1:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getShortestPathTo(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.minDistance);
													tempTotal = v.minDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										case 2:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getLongestPath(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.maxDistance);
													tempTotal = v.maxDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										default:
											System.out.println("Error | Invalid Menu");
									};
								}else System.out.println("\nError | Tujuan tidak valid!");
								
							break;
							default:
								System.out.println("Error | Invalid Menu");
							break;

						}
					break;
					case 2:
						driver.getStatus();
					break;
					case 3:
						driver.getWishList();
					break;
					case 4:
						driver.changeState();
					break;
					default:
						System.out.println("Error | Invalid Menu ");
					break;
				}
			break;
			case MngVertex.FOODSHOP :
				System.out.println("\n\nToko Makanan");
				System.out.println("~Posisi: " + driver.getCurrent().nama+"");
				System.out.println("  1. Pembelian\n  2. Lanjutkan Perjalanan\n  3. Cek Status\n  4. Cek Pesanan\n  5. Ubah Mode");
				System.out.print("~Pilih : ");
				menu = getInt();

				switch(menu){
					case 1:
						int selectMenu = 0;
						do{
							System.out.println("\n~Pembelian~~~");
							FoodShop sFoodShop = getFoodShop(driver.getCurrent());
							int in = 1;
							for(Menu m : sFoodShop.menu){
								System.out.println(in++ +". " + m.nama + " " + m.harga);
							}
							System.out.print("~Pilih (0 untuk batal) :");
							selectMenu = getInt()-1;
							if(selectMenu!=-1){
								System.out.print("~Jumlah :");
								int selectJumlah = getInt();

								driver.buyItem(sFoodShop.menu.get(selectMenu), selectJumlah);
							}
						}while(selectMenu!=-1);
						
					break;
					case 2:
						switch(driver.getMode()){
							case 1:
								System.out.println("\n~Pengemudi Manual ~~~");
								tempDestination = graph.getDestinationFrom(driver.getCurrent());
								printDestination(tempDestination);
								System.out.print("~Pilih : ");
								int select = getInt()-1;
								driver.setPosition(tempDestination.get(select));
							break;
							case 2:
								System.out.println("\n~Pengemudi Otomatis ~~~");
								System.out.println("   1. Toko Makanan\n   2. Pelanggan\n   3. Persimpangan\n   4. ABCorp");
								System.out.print("~Pilih : ");
								menu = getInt();
								tempDestinationVertex = graph.getArrayLocation(menu, driver.getCurrent());
								System.out.println("\n~ Tujan ~~~~");
								printDestinationVertex(tempDestinationVertex);
								System.out.print("~Pilih : ");
								int select2 = getInt()-1;
								if(select2 >=0 && select2 <tempDestinationVertex.size()){
									System.out.println("\n~Pilih Driver:\n   1. Driver Cepat\n   2. Driver Lambat");
									System.out.print("~Pilih : ");
									int select3 = getInt();
									double tempTotal=0;
									switch(select3){
										case 1:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getShortestPathTo(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.minDistance);
													tempTotal = v.minDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										case 2:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getLongestPath(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.maxDistance);
													tempTotal = v.maxDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										default:
											System.out.println("Error | Invalid Menu");
									};
								}else System.out.println("\nError | Tujuan tidak valid!");
								
							break;
							default:
								System.out.println("Error | Invalid Menu");
							break;

						}
					break;
					case 3:
						driver.getStatus();
					break;
					case 4:
						driver.getWishList();
					break;
					case 5:
						driver.changeState();
					break;
					default:
						System.out.println("Error | Invalid Vertex ");
					break;
				};
			break;
			case MngVertex.CUSTOMER:
				System.out.println("\n\nPelanggan");
				System.out.println("~Posisi : "+ driver.getCurrent().nama + "\n");
				System.out.println("  1. Antar Pesanan\n  2. Lanjutkan Perjalanan\n  3. Cek Status\n  4. Cek Pesanan\n  5. Ubah Mode");
				System.out.print("~Pilih : ");
				menu = getInt();

				switch(menu){
					case 1:
						
						driver.serveCustomer();
					break;
					case 2:
						switch(driver.getMode()){
							case 1:
								System.out.println("\n~Pengemudi Manual ~~~");
								tempDestination = graph.getDestinationFrom(driver.getCurrent());
								printDestination(tempDestination);
								System.out.print("~Pilih : ");
								int select = getInt()-1;
								driver.setPosition(tempDestination.get(select));
							break;
							case 2:
								System.out.println("\n~Pengemudi Otomatis ~~~");
								System.out.println("   1. Toko Makanan\n   2. Pelanggan\n   3. Persimpangan\n   4. ABCorp");
								System.out.print("~Pilih : ");
								menu = getInt();
								tempDestinationVertex = graph.getArrayLocation(menu, driver.getCurrent());
								System.out.println("\n~ Tujan ~~~~");
								printDestinationVertex(tempDestinationVertex);
								System.out.print("~Pilih : ");
								int select2 = getInt()-1;
								if(select2 >=0 && select2 <tempDestinationVertex.size()){
									System.out.println("\n~Pilih Driver:\n   1. Driver Cepat\n   2. Driver Lambat");
									System.out.print("~Pilih : ");
									double tempTotal=0;
									int select3 = getInt();
									switch(select3){
										case 1:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getShortestPathTo(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.minDistance);
													tempTotal = v.minDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										case 2:
										System.out.println("\n~ Rute ~~~\n");
											tempRoute = graph.getLongestPath(driver.getCurrent(), tempDestinationVertex.get(select2));
											System.out.println("Start -> ");
											for(Vertex v : tempRoute){
												System.out.println("- " + v.nama);
												if(v == tempDestinationVertex.get(select2)) {
													driver.setPosition(v, v.maxDistance);
													tempTotal = v.maxDistance;
												}
											}
											System.out.println("End;");
											System.out.println("~Total Jarak : " + tempTotal);
										break;
										default:
											System.out.println("Error | Invalid Menu");
									};
								}else System.out.println("\nError | Tujuan tidak valid!");
								
							break;
							default:
								System.out.println("Error | Invalid Menu");
							break;

						}
					break;
					case 3:
						driver.getStatus();
					break;
					case 4:
						driver.getWishList();
					break;
					case 5:
						driver.changeState();
					break;
					default:
						System.out.println("Error | Invalid Menu");
					break;
				};		
			break;
			default:
				System.out.println("Error | Invalid Menu ");
			break;

		}
		}
	}

	public static int getInt(){
		boolean check = false;
		do{
			try{
				Scanner input = new Scanner(System.in);
				return input.nextInt();
			}catch(Exception e){
				System.out.println("Error | Invalid Input ");
				check = false;
			}
		}while(!check);
		return 0;
	}

	public static void printDestinationVertex(ArrayList<Vertex> vertex){
		int i = 1;
		for(Vertex v : vertex){
			System.out.println(i + ". " + v.nama);
			i++;
		}
	}
	
	public static void printDestination(ArrayList<Edge> edges){
		int i = 1;
		for(Edge e : edges){
			System.out.println(i + ". " + e.destination.nama);
			i++;
		}
	}

	public static FoodShop getFoodShop(Vertex v){
		return (FoodShop) v;
	}
	
}
